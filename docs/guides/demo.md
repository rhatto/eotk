# Demonstration and Testing

## Trying yourself

After installation, you can do:

* Configure a demo by running either
  * `./onionspray config examples/wikipedia.tconf`
  * or: `./onionspray config examples/wikipedia-v3.tconf`
* Start the demo:
  * `./onionspray start wikipedia`
* And connect to one of the onions you've created:
  * `./onionspray maps -a`

Be aware that you will suffer from HTTPS certificate errors
until you [get a HTTPS certificate](certificates.md).

## Video Demonstrations

**These videos are instructive, but dated.**

Commands have changed - but not very much - but please check the
documentation rather than trust the videos; consider the videos to be
advisory rather than correct.

* [Basic Introduction to EOTK](https://www.youtube.com/watch?v=ti_VkVmE3J4)
* [Rough Edges: SSL Certificates & Strange Behaviour](https://www.youtube.com/watch?v=UieLTllLPlQ)
* [Using Onionbalance](https://www.youtube.com/watch?v=HNJaMNVCb-U)

[![Basic Introduction to EOTK](http://img.youtube.com/vi/ti_VkVmE3J4/0.jpg)](http://www.youtube.com/watch?v=ti_VkVmE3J4)
[![Rough Edges: SSL Certificates & Strange Behaviour](http://img.youtube.com/vi/UieLTllLPlQ/0.jpg)](http://www.youtube.com/watch?v=UieLTllLPlQ)
[![Using Onionbalance](http://img.youtube.com/vi/HNJaMNVCb-U/0.jpg)](http://www.youtube.com/watch?v=HNJaMNVCb-U)
