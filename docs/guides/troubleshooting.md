# Troubleshooting

## Basic check

If something is problematic, first try:

* `git pull` and...
* `onionspray config <filename>.conf` again, and then...
* `onionspray bounce -a`

## Logs

The logs for any given project will reside in
`projects/<PROJECTNAME>.d/logs.d/`

## Lots Of Broken Images, Missing Images, Missing CSS

This is probably an SSL/HTTPS thing.

Because of the nature of SSL self-signed certificates, you have to manually
accept the certificate of each and every site for which a certificate has been
created. See the second of the YouTube videos for some mention of this.

In short: this is normal and expected behaviour.  You can temporarily fix this
by:

* Right-clicking on the image for `Open In New Tab`, and accepting the
  certificate
* Or using `Inspect Element > Network` to find broken resources, and doing the
  same
* Or -- if you know the list of domains in advance - visiting the
  `/hello-onion/` URL for each of them, in advance, to accept certificates.

If you get an [official SSL certificate for your onion
site](https://blog.digicert.com/ordering-a-onion-certificate-from-digicert/)
then the problem will vanish. Until then, I am afraid that you will be stuck
playing certificate "whack-a-mole".

## NGINX: Bad Gateway

Generally this means that NGINX cannot connect to the remote website, which
usually happens because:

* the site name in the config file, is wrong
* the nginx daemon tries to do a DNS resolution, which fails

Check the NGINX logfiles in the directory cited above, for confirmation.

If DNS resolution is failing, *PROBABLY* the cause is probably lack of access
to Google DNS / 8.8.8.8; therefore in your config file you should add a line
like this - to use `localhost` as an example:

    set nginx_resolver 127.0.0.1

... and then do:

    onionspray stop -a
    onionspray config filename.conf
    onionspray start -a

If you need a local DNS resolver, I recommend `dnsmasq`.

## I Can't Connect, It's Just Hanging!

If your onion project has just started, it can take up to a few minutes to
connect for the first time; also sometimes Tor Browser caches stale descriptors
for older onions.  Try restarting Tor Browser (or use the `New Identity` menu
item) and have a cup of tea.  If it persists, check the logfiles.

## Onionbalance Runs For A Few Days, And Then Just Stops Responding!

Is the clock/time of day correct on all your machines?  Are you running NTP?
We are not sure but having an incorrect clock may be a contributory factor to
this issue.
