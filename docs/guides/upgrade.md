# Upgrading procedure

Upgrading Onionspray can be done in many ways, an in this guide we mention only
a few.

!!! warning "Always backup before upgrading"

    In any case, always [backup](backup.md) your setup before doing
    an upgrade.

!!! warning "Read the ChangeLog"

    Always check the [ChangeLog](../changelog.md) before doing an upgrade,
    especially paying attention to any breaking change that may impact
    your setup. There you can find specific procedures to handle these
    breaking changes.

## Simpler approach: start all over

**If unsure, get a fresh copy of the [Onionspray repository][]**:

* First, stop the services.
* Then move the existing installation away to some other location (say from
  `/srv/onionspray` to `/srv/onionspray.20240207`).
* Then clone the [Onionspray repository][] into the desired local (say `/srv/onionspray`).
* Proceed with the [installation](installation.md).
* Import your existing .onion keys, HTTPS certificates and configuration files
  from the previous installation.
* Reconfigure all your projects.
* Start everything.

[Onionspray repository]: https://gitlab.torproject.org/tpo/onion-services/onionspray

## Usual approach: upgrade an existing installation

If you prefer, you can upgrade an existing installation in place after [backing
up](backup.md) and reading the [ChangeLog](../changelog.md).

Go to the Onionspray folder and upgrade the Git repository working copy:

    cd /path/to/onionspray
    git pull

You can also checkout Onionspray in a specific release/revision.

Optionally upgrade the installation by running the script related to your
Operating System and method. This is useful if the new Onionspray version
relies on updated upstream binaries, and you can safely skip this if you're
relying on upstream packages. Example:

    ./opt/build-debian-bullseye.sh

Then reconfigure each existing project:

    ./onionspray config myproject.conf

Finally, restart all your projects:

    ./onionspray restart -a

If you're running [softmaps](balance/softmaps.md), please also restart
[Onionbalance][]:

    ./onionspray ob-restart -a

[Onionbalance]: https://onionservices.torproject.org/apps/base/onionbalance/
