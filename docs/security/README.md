# Onionspray security model

This section has security analysis and advisories about Onionspray.

## How safe is an Onion Service rewriting proxy?

It is generally safe to run an Onion Service proxy for your public websites,
as long as you make sure to properly configure it and understand it's limits.

### Make sure that HTTPS certificates are always validated

!!! danger "Upstream HTTPS certificate validity"

    Configure Onionspray to always check the upstream HTTPS certificate.

    This can be done with the `nginx_proxy_ssl_trusted_certificate` setting,
    that should be configured in every project.

The backend proxy connection relies on HTTPS with properly validated
certificates:

* This is enforced for Onionspray projects configured with the
  `nginx_proxy_ssl_trusted_certificate` setting, which makes the
  rewriting proxy to only connect to HTTPS endpoints which it
  can properly validate the certificate. This will protect
  the Onion Service from many machine-in-the-middle (MITM)
  attacks. More information on this attack vector is
  detailed in [this security advisory](advisories/002-proxy_ssl_verify.md).

* But this won't protect it from domain and/or Certificate Authority (CA)
  hijacks.  If the domain and/or the CA is hijacked, rogue certificates can be
  generated and attackers can point the upstream domain to a rogue site.

### Make sure to properly configure the DNS

To increase protection against impersonation, it can be desirable to ensure
the rewriting proxy is using an appropriate DNS resolver:

* In Onionspray projects, this can be achieved with the `nginx_resolver`
  configuration setting. You can then point the rewriting proxy
  to use a DNS proxy that properly checks domain entries, relying on
  technologies such as DNSSEC and DNS over TLS or HTTPS.

* You can further increase the security of your upstream, public DNS domain by
  setting up DNSSEC.

* If you really want finer control, you can set `nginx_resolver` to
  a customized DNS resolver that has a hardcoded or internal view
  where you can manually set the IP addresses for the upstream site.

### Make sure users are aware of the official .onion address

!!! danger "Inform users about the official .onion"

    Properly communicate to your users about the official .onion
    address.

Impersonation attacks can also work through social engineering, by convincing
users to use rogue .onion addresses impersonating the official website:

* It's easy to run an Onion Service rewriting proxy for public websites.
  In fact anybody can do it, and in a few minutes it's possible to do it
  with Onionspray, and and it's also easy to make a rewriting proxy
  that goes beyond fixing URLs and also tampers with the content.

* The only thing that protects such impersionation is to make sure
  users are browsing the correct site.

There are many ways to inform your users about the correct .onion
addresses to use, especially by setting [Onion Location][] in the
regular site.

There are also some [usability proposals][] to improve this.

[Onion Location]: https://community.torproject.org/onion-services/advanced/onion-location/
[usability proposals]: https://tpo.pages.torproject.net/onion-services/onionplan/proposals/usability/discovery/

### Use UNIX sockets whenever possible

In Onionspray, the connection between [C Tor][] daemon processes and the
rewriting proxies are done through UNIX sockets with proper ownership and
permissions, to make it more difficult to tamper or eavesdrop on this
connection.

[C Tor]: https://gitlab.torproject.org/tpo/core/tor
