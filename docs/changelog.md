# Onionspray ChangeLog

## v1.6.3 - Unreleased

* Support building Onionspray without `sudo` privileges on Debian-like systems
  ([tpo/onion-services/onionspray#61][]).

[tpo/onion-services/onionspray#61]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/61

## v1.6.2 - 2024-11-27

* Bump Tor to 0.4.8.13.

* Bump OpenResty to 1.25.3.2.

* Update the self-signed cert script to use SHA384 digest and the secp384r1
  curve.

## v1.6.1 - 2024-03-26

This is a maintenance release, with some fixes and a few minor underlying features.

Operators are advised to reconfigure and restart their services to apply
template updates.

### Fixes

* Updated `lint.pl` with recently added options.
* Run lint during CI/CD.
* Standardize build/install script names.
* Use timestamps in the configuration log file names (closes [tpo/onion-services/onionspray#46][]).
* Build script improvements ([tpo/onion-services/onionspray!81][]).
* Fix the `tor` daemon templates, to properly set per-service options ([tpo/onion-services/onionspray!82][]).
  This affects the DoS-related settings and `tor_export_circuit_id`.

[tpo/onion-services/onionspray#46]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/46
[tpo/onion-services/onionspray!81]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/merge_requests/81
[tpo/onion-services/onionspray!82]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/merge_requests/82

## v1.6.0 - 2024-02-09

Version 1.6.0 is the first Onionspray release after forking it from [EOTK][]
and completing its [first milestone][].

[EOTK]: https://github.com/alecmuffett/eotk
[first milestone]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/milestones/1

### Security fixes

* **CRITICAL**: unpatched Onionspray and [EOTK][] are not verifying the upstream TLS certificate:
    * This Onionspray release fixes an important security issue in Onionspray
      proxy layer, and which also affects all unpatched versions of [EOTK][].
    * A patch for [EOTK][] is also available.
    * All Onionspray and [EOTK][] users are urged to upgrade or apply the
      patch.
    * Besides upgrading their installations with the fix, operators also need to update the
      configuration for all their projects to ensure the `nginx_proxy_ssl_trusted_certificate`
      is in place. Then they need to reconfigure and restart all projects.
    * Details are available on the related [security advisory](security/advisories/002-proxy_ssl_verify.md).

### Features and improvements

These are the main improvements over [EOTK][]:

* Denial of Service (DoS) protections were implemented.
    * Closes:
      * [Setup DoS defenses for EOTK (PoW, Introduction Points and Stream-based) (#6)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/6)

* MetricsPort support (for gathering metrics data from the tor instances).
    * Right now this is only working for `hardmap`, as `softmap` uses many
      `tor` daemon instances causing a conflict when trying to open the same
      metrics port. Issue being tracked on ticket [#43](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/43).
    * Closes:
        * [MetricsPort support (#35)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/35)

* Circuit ID exporting to NGINX logs and optionally to the upstream
  proxy through the X-Onion-CircuitID HTTP header.
    * Closes:
        * [Support for logging Onion Service circuit IDs (#7)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/7)
        * [Set the Onion Services Circuit ID in a HTTP header (#42)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/42)

* Revamped the documentation:
    * With Onion MkDocs website at https://tpo.pages.torproject.net/onion-services/onionspray/.
    * Closes:
        * [Documentation cleanup (#14)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/14)
        * [Setup Onion MkDocs (#15)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/15)
        * [Improve and cleanup the documentation (#17)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/17)
        * [Fine tune the documentation (#19)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/19)
        * [Improve the Load Balancing docs (#20)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/20)
        * [Create a simple logo (#29)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/29)
        * [Create a basic hardmap guide (#30)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/30)
        * [Minor documentation fixes (#34)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/34)
        * [Update mining documentation (#39)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/39)

* Installation procedures added for recent Debian and Ubuntu releases.
    * Closes:
        * [Build scripts for Debian bullseye and bookworm (#3)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/3)
        * [Build script for recent Ubuntu LTS (#8)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/8)

* Tor and OpenResty upgraded to the latest versions.
    * Closes:
        * [Upgrade Tor from 0.4.5.8 to 0.4.8.10 (#4)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/4)
        * [Upgrade OpenResty to 1.25.3.1 (#10)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/10)

* Basic CI.
    * Closes:
        * [Setup CI/CD for EOTK (#2)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/2)
        * [CI testing builds for many distros/versions (#9)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/9)

### Command line options

* Added option to keep Onionspray running in the foreground (`--no-daemonize`),
  useful for containerized environments.
    * Fixes:
        * [Option to keep Onionspray running in the foreground (#40)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/40)

* Added a local health check action (`health-local` or `healthcheck-local`).
    * Fixes:
        * [Healthcheck action (#41)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/41)

* Added `ob-bounce` and `ob-reload` as aliases to `ob-restart` (commit [905db7cd4e4b388a18b4bb203483e09878b54402][]).

### Fixes

* Onionbalance v3 support was fixed ("softmaps" are working again).
    * Closes:
        * [Softmap working example and CI (#22)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/22)

* Avoid auto-echoing paths on "cd" commands.
    * Closes:
        * [Avoid auto-echoing paths on "cd" commands (#5)](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/5)

* `./onionspray ps` command now outputs the `tor` daemon processes (commit
  [b130bcc80297f3625fe7d291abf81ed9990ec4ea][]).

[b130bcc80297f3625fe7d291abf81ed9990ec4ea]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/commit/b130bcc80297f3625fe7d291abf81ed9990ec4ea
[905db7cd4e4b388a18b4bb203483e09878b54402]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/commit/905db7cd4e4b388a18b4bb203483e09878b54402

### Breaking changes

#### EOTK forked and renamed to Onionspray

Onionspray project starts as a fork of the [Enterprise Onion Toolkit (EOTK)][].
Details of the forking and renaming process can be found in these tickets:

* [tpo/onion-services/onionspray#1][].
* [tpo/onion-services/onionspray#13][].

Compatibility with [EOTK][] is kept: configuration, keys and certificates can
be easily [migrated](migrating.md) from [EOTK][], and the `onionspray` command
it compatible with the `eotk` script (which is kept as an alias).

A migration procedure is available in [this tutorial](migrating.md).

[Enterprise Onion Toolkit (EOTK)]: https://github.com/alecmuffett/eotk
[tpo/onion-services/onionspray#1]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/1
[tpo/onion-services/onionspray#13]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/13

#### New folder structure

* Onionspray has a slightly different folder structure than [EOTK][].
  In particular, the traditional `.d` suffix in folder names were
  removed (check [#24](https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/24)).

#### HTTPS support (from 2022-03 onwards)

There was a small breaking change in order to better-support HARICA
as a certificate provider, but also for better usability; this
change impacts any project with a multi-onion EV certificate from
Digicert.

* v3 onion addresses used in pathnames are now truncated at 20 chars
  of onion, rather than 30 overall, to make shorter pathnames for unix
  domain sockets
* onion scratch-directory name changes:
    * was: `projects/tweep.d/abcdefghijklmnopqrstuvwxyza-v3.d/port-80.sock`
    * now: `projects/tweep.d/abcdefghijklmnopqrst-v3.d/port-80.sock`
    * this means that some scratch directories may be are remade,
    so a full restart is advisable after updating
* https certificate path-name changes
    * was: HTTPS certificate files used the full onion address
    * now: onion HTTPS certificates are now expected to be installed in
    per-onion-truncated-at-20 pathnames: e.g. for each ONIONADDRESS in
    PROJECTNAME:
        * `/projects/PROJECTNAME.d/ssl.d/ONIONADDRFIRST20CHAR-v3.onion.cert`
        * `/projects/PROJECTNAME.d/ssl.d/ONIONADDRFIRST20CHAR-v3.onion.pem`
    * this means that you will need to rename pre-existing certificate
    `cert` and `pem` files after you update and reconfigure;
    * **if you fail to do this you will experience "self-signed certificate" warnings**
* if you are using 'multi' certificates (such as some Digicert EV) where a
  single certificate contains all SubjectAltNames for 2+ onion
  addresses that are part of a single project:
    * do `set ssl_cert_each_onion 0` in the configuration, to re-enable
    multi cert handling
    * the names of the certificate files must be changed:
        * was: filenames used to be
        `projects/PROJECTNAME.d/ssl.d/PRIMARYONIONADDRESSWASHERE.{pem,cert}`
        * now: multi-certificates now must be named with the more meaningful
        `projects/PROJECTNAME.d/ssl.d/PROJECTNAME.{pem,cert}`

If you have any issues, please fill a ticket.

## v1.5.0 - 2021-05-18

* new features
* `eotk-site.conf`
  * "global" configuration rules to prefix into every configuration
  * auto-created it if it does not exist
  * used it to solve NGINX installation on Ubuntu 18.04LTS
* Onionbalance to be deprecated until overhauled by Tor
  * see notes in the [load balancing introduction](guides/balance/README.md)
* Ubuntu 18.04LTS becomes mainline for Ubuntu
  * Ubuntu 16.04LTS becomes deprecated
* Support for v3 Onion Addresses
  * use `%NEW_V3_ONION%` in template configs (`.tconf`) to autogenerate
  * see `examples/wikipedia-v3.tconf` for examples

## v1.4.0 - 2019-06-27

* new features
  * auto-generate (`eotk make-scripts`) wrapper scripts for:
    * installation into "init" startup `eotk-init.sh`
    * log rotation / housekeeping via installation into eotk-user cronjob (`eotk-housekeeping.sh`)
  * stricter enforcement of onionification
    * by default will drop compressed, non-onionifiable content with error code `520`
    * if this is a problem (why?) use `set drop_unrewritable_content 0` to disable
  * validate worker onion addresses upon creation
    * works around [issue logged with tor](https://trac.torproject.org/projects/tor/ticket/29429)
  * `eotk spotpush` now permits pushing explicitly named scripts from `$EOTK_HOME`, as well as hunting in `projects`
  * `set hard_mode 1` is now **default** and onionifies both `foo.com` AND `foo\.com` (regexp-style)
    * use `set hard_mode 2` to further onionify `foo\\.com` and `foo\\\.com` at slight performance cost
  * `set ssl_mkcert 1` to use [mkcert](https://github.com/FiloSottile/mkcert) by @FiloSottile for certificate generation, if you have installed it
  * refactor nonces used in rewriting preserved strings
  * improvements to `set debug_trap pattern [...]` logging
  * support for OpenResty LuaJIT in Raspbian build scripts
  * update code version for Raspbian builds scripts
  * tor HiddenServiceVersion nits
  * dead code removal
  * deprecate support for Raspbian Jessie

## v1.3.0 - 2017-04-14

* new features
  * "Runbook" has been [moved to the documentation directory](https://github.com/alecmuffett/eotk/blob/master/docs.d/deprecated-documents.d/SOFTMAP-RUNBOOK.md)
  * tor2web has been blocked-by-default
    * since the reason for EOTK is to provide Clearnet websites with an Onion presence, Tor2web is not necessary
  * the `FORCE_HTTPS` feature has been added and made *default*
    * if your site is 100% HTTPS then you do not need to do anything,
    * however sites which require insecure `HTTP` may have to use `set force_https 0` in configurations.

## v1.2.0 - 2017-02-04?

* new features:
  * optional blocks to methods other than GET/HEAD
  * optional 403/Forbidden blocks for accesses to certain Locations or Hosts, including as regexps
    * nb: all blocks/block-patterns are *global* and apply to all servers in a project
  * optional time-based caching of static content for `N` seconds, with selectable cache size (def: 16Mb)
* new [How To Install](guides/installation.md) guide
* custom install process for Ubuntu, tested on Ubuntu Server 16.04.2-LTS
* renaming / factor-out of Raspbian install code
* fixes to onionbalance support

## v1.1.0 - 2017-04-04

* first cut of onionbalance / softmap

## v1.0.0 - 2017-02-12

* have declared a stable alpha release
* architecture images, at bottom of this page
* all of CSP, HSTS and HPKP are suppressed by default; onion networking mitigates much of this
* ["tunables"](guides/templates.md) documentation for template content
* `troubleshooting` section near the bottom of this page
* See [project activity](https://github.com/alecmuffett/eotk/graphs/commit-activity) for information
